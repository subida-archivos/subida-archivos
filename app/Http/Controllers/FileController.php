<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use App\Traits\UploadFileTrait;
use App\Http\Requests\FileRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class FileController extends Controller
{
    use UploadFileTrait;
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $files = File::all();
        return response()->json([
            'status' => true,
            'data' => $files
        ], Response::HTTP_OK);
    }

    public function upload(Request $request)
    {
        $validate = $this->validateFile($request);
        if (!$validate) {
            if ($request->hasFile('name_file')) {
                $files = $request->file('name_file');
                return $this->uploadFile($files);
            }
        }
        return $validate;
    }

    public function destroy($id)
    {
        // Obtenemos el archivo que vamos a eliminar
        $file = File::where('id',$id)->firstOrFail();
        return $this->deleteFile($file);
        // eliminamos fichero de forma logica y fisica

    }
}
