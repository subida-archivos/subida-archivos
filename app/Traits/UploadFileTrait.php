<?php
namespace App\Traits;

use App\Models\File;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 */
trait UploadFileTrait
{
    public function uploadFile($files)
    {
        foreach($files as $key => $file){
            $path = $file->store('public/files');
            $name = $file->getClientOriginalName();
            File::create([
                'name_file' => $name,
                'path' => $path
            ]);
        }
        return response()->json([
            'status' => true,
            'message' => "Archivo cargado exitosamente",
        ], RESPONSE::HTTP_OK);
    }

    public function validateFile($request)
    {
        foreach ($request->name_file as $key => $file) {
            $name = $file->getClientOriginalName();//Obtenemos nombre del archivo
            $data = [
                'name_file' => $request->name_file[$key]
            ];
            $validate = Validator::make($data, [
                'name_file' => 'required|max:500'
            ]);
            if($validate->fails()){
                return ['message' => 'El archivo ' .$name.' no debe pesar más de 500kb'];
            }
        }
    }

    public function deleteFile($file)
    {
        Storage::delete($file->path);
            $file->delete();
            return response()->json([
                'status' => true,
                'message' => 'Archivo elimiado'
            ], RESPONSE::HTTP_OK);
    }
}

